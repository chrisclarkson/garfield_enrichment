diseases=read.table('~/scrape_umls/CVD_types',header=F,stringsAsFactors=F)$V1
diseases=diseases[!(diseases %in% c('HF','AAA'))]
print(diseases)
bios=read.table('~/scrape_umls/bios',header=T,stringsAsFactors=F)$Biomarkers
dir='/lustre/projects/DTAdb/resources/garfield/garfield-data/output/'
data=read.table(paste0(dir,'CVD_Apolipoprotein_B/garfield.test.CVD_Apolipoprotein_B.out.peaks_data.txt'),sep='\t',header=T,stringsAsFactors=F)
tissues=unique(data$tissues)
full_mat=0:length(tissues)
for(dis in diseases){
    mat=matrix(ncol=length(tissues),nrow=length(bios))
    colnames(mat)=tissues
    rownames(mat)=bios
    mat=cbind(paste0(dis),mat)
    for(bio in bios){
        file=paste0(dir,dis,'_',bio,'/','garfield.test.',dis,'_',bio,'.out.peaks_data.txt')
        if(file.exists(file)){
            data=read.table(file,sep='\t',header=T,stringsAsFactors=F)
            tis=unique(data$tissues)
            for(t in tis){
                data_sub=data[data$tissues==t,]
                if(nrow(data_sub)>1){
                    mat[bio,t]=sum(data_sub$enrichment)
                }else{
                    mat[bio,t]=data_sub$enrichment
                }
            }
            full_mat=rbind(full_mat,mat)
    }}
}
diseases=full_mat[-1,1]
full_mat=full_mat[,-1]
full_mat=full_mat[-1,]


full_mat[is.na(full_mat)]=0
bios=rownames(full_mat)
full_mat=apply(full_mat,2,as.numeric)
rownames(full_mat)=bios
library(ComplexHeatmap)
png('tissue_enrichment.png',height=1000,width=1000)
Heatmap(full_mat,name='-Log 10 P-value Enrichment',
        cluster_rows=F,show_row_names = F,row_names_gp = gpar(fontsize = 2),
        cluster_columns=F,row_split=diseases
    )
dev.off()

